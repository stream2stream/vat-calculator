import { useState } from 'react';

import './App.css';
import PriceEntryField from './PriceEditor';
import CDisplayBlock from './DisplayBlock';
import CVATField from './VATField';

function App() {
  const [vatToPay, setVATToPay] = useState(0.0);
  const [salePrice, setSalePrice] = useState(0.0);
  const [totalPrice, setTotalPrice] = useState(0.0);
  const [vatRate, updateVATRate] = useState(20.00);

  const handleExclPriceChange = (price) =>{
    const totalLocalPrice = price * ((vatRate / 100) + 1);
    setSalePrice(price)
    setTotalPrice( totalLocalPrice );
    updateVatToPay(price, totalLocalPrice);
    console.log("handleExclPriceChange")
  };

  const handleInclPriceChange = (price) =>{
    const saleLocalPrice = price / ((vatRate / 100) + 1);
    setTotalPrice(price)
    setSalePrice( saleLocalPrice );
    updateVatToPay(price, saleLocalPrice);
    console.log("handleInclPriceChange")
  };

  const handleVATChargeRate =  () =>{
    handleExclPriceChange( salePrice );
  };

  const updateVatToPay = (priceWithoutVAT, priceWithVAT) =>
  {
    setVATToPay(priceWithVAT-priceWithoutVAT);
  }


  return (
    <div>
      VAT CALCULATOR
      <div className="App pale-green-border">
        <CVATField cstyle="left-align" vatRateChanged= {updateVATRate} currentVATRate={vatRate} updatePrices={handleVATChargeRate}/>
        <p></p>
        <PriceEntryField cstyle="left-align" label="Price excl VAT: " priceChanged={handleExclPriceChange} price={salePrice===0.0 ? "" : salePrice}/>
        <p></p>
        <CDisplayBlock cstyle="left-align" label="VAT to Pay: " value={vatToPay}/>
        <p></p>
        <PriceEntryField cstyle="left-align" label="Price incl VAT: " priceChanged={handleInclPriceChange} price={totalPrice===0.0 ? "" : totalPrice}/>
      </div>
  </div>

  );
}

export default App;
