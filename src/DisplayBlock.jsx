const CDisplayBlock = (props) =>
{
    return (
        <div className={props.cstyle}>
            {props.label} {props.value}
        </div>
    )
}

export default CDisplayBlock;