import React from 'react';

const CEditorField = (props) =>
{
    return (
        <input  type="number" 
                id="lname" 
                name="lname" 
                value={props.value}
                onChange={(event)=>{
                    props.valueChanged(+event.target.value);
                }
        }/>
    )
}



export default CEditorField;