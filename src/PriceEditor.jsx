import {React} from 'react';
import CEditorField from './EditorField';

const PriceEntryField = (props) =>
{
    return (
        <div className={props.cstyle}>
            {props.label}
            <CEditorField value={props.price} valueChanged={props.priceChanged}/>
        </div>
    )
}

export default PriceEntryField;