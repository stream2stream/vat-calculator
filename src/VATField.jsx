import { useEffect, useState } from "react";

const CVATField = (props) =>
{
    const [vatRate, updateVATRate] = useState(20);

    const {updatePrices} = props;

    useEffect(() => {
        updatePrices();
    }, [vatRate, updatePrices]);

    return(
        <div className={props.cstyle}>
            VAT Rate: 
            <select onChange={(event)=>{
                    updateVATRate(+event.target.value);
                    props.vatRateChanged(+event.target.value)
                }}> 
                <option value="20">20%</option> 
                <option value="15">15%</option> 
                <option value="12.5">12.5%</option> 
                <option value="0">Exempt</option> 
            </select> 
        </div>
    );
}

export default  CVATField